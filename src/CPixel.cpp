/*
 * CPixel.cpp
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#include "CPixel.h"

CPixel::CPixel()
:alive(true)
,x(0)
,y(0)
,dir(0)
{

}

CPixel::~CPixel() {

}

int CPixel::Move(CMatrix& matrix){
	return 1;
}

void CPixel::Disappear(){
	alive = false;
}

int CPixel::GetX(){
	return x;
}

int CPixel::GetY(){
	return y;
}

void CPixel::SetX(int newX){
	x = newX;
}

void CPixel::SetY(int newY){
	y = newY;
}

bool CPixel::IsAlive(){
	return alive;
}

int CPixel::GetDirection(){
	return dir;;
}

void CPixel::SetDirection(int newDir){
	dir = newDir;
}
