/*
 * CGhost.cpp
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#include "CGhost.h"
#include <cstdlib>
#include <ctime>

CGhost::CGhost()
	:onFood(false){
	std::srand(time(NULL));
}

CGhost::~CGhost() {

}

void CGhost::StepOnFood(bool doesHe){
	onFood = doesHe;
}

bool CGhost::IsOnFood(){
	return onFood;
}

int CGhost::Move(CMatrix& matrix){
	int tmp;
	bool tryDir[4];
	for(int i = 0; i < 4; i++)
		tryDir[i] = true;
	tryDir[(dir+2)%4] = false;

	while(tryDir[0] || tryDir[1] || tryDir[2] || tryDir[3]){
		tmp = std::rand() % 4;

		switch(tmp){
		case 0:
			if(tryDir[0] && matrix.WhatsHere(y-1, x) != '#')
				return tmp;
			else
				tryDir[0] = false;
			break;
		case 1:
			if(tryDir[1] && matrix.WhatsHere(y, x+1) != '#')
				return tmp;
			else
				tryDir[1] = false;
			break;
		case 2:
			if(tryDir[2] && matrix.WhatsHere(y+1, x) != '#')
				return tmp;
			else
				tryDir[2] = false;
			break;
		case 3:
			if(tryDir[3] && matrix.WhatsHere(y, x-1) != '#')
				return tmp;
			else
				tryDir[3] = false;
			break;
		}
	}
	return (dir+2)%4;
}

