/*
 * CScheduler.h
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#ifndef CSCHEDULER_H_
#define CSCHEDULER_H_

#include "CMatrix.h"
#include <ncurses.h>

class CScheduler {
public:
	CScheduler();
	~CScheduler();

	void Run(WINDOW*, WINDOW*, CMatrix&);
private:
	void Win(WINDOW* infoBox);
	void Lose(WINDOW* infoBox);
};

#endif /* CSCHEDULER_H_ */
