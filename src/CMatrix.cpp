/*
 * CMatrix.cpp
 *
 *  Created on: 28.06.2017
 *      Author: luke
 */

#include <fstream>
#include <string>
#include "CMatrix.h"
#include <ncurses.h>
using namespace std;

CMatrix::CMatrix()
	:matrix(NULL)
	,gameBox(NULL)
	,rows(0)
	,columns(0)
	,foodLeft(0)
{

}

CMatrix::~CMatrix() {
	for(int i = 0; i < rows; i++)
		delete[] matrix[i];
	delete[] matrix;
}

void CMatrix::LoadMatrixFromFile(){
	ifstream file;
	ActualFilePath = GetNewFilePath();
	file.open(ActualFilePath.c_str());
	file.exceptions(file.failbit);
	file.exceptions(file.goodbit);

	string line;

	getline(file, line);
	rows++;
	columns = line.length();
	while(getline(file, line)){
		rows++;
	}

	file.clear();
	file.seekg(0);
	matrix = new char*[rows];
	for(int i = 0; i < rows; i++){
		matrix[i] = new char[columns];
		getline(file, line);
		for(int j = 0; j < columns; j++)
			matrix[i][j] = line.at(j);
	}
	file.close();

}

void CMatrix::ReloadMatrix(int dirP, int dirG){
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < columns; j++)
			switch(matrix[i][j]){
			case '#':
				mvwaddch(gameBox, i, j, ACS_CKBOARD);
				break;
			case 'x':
			case '.':
				mvwaddch(gameBox, i, j, ACS_DIAMOND);
				break;
			case ' ':
				mvwaddch(gameBox, i, j, ' ');
				break;
			case 'P':
				wattron(gameBox, COLOR_PAIR(4));
				switch(dirP){
					case 0:
						mvwaddch(gameBox, i, j, ACS_UARROW);
						break;
					case 1:
						mvwaddch(gameBox, i, j, ACS_RARROW);
						break;
					case 2:
						mvwaddch(gameBox, i, j, ACS_DARROW);
						break;
					case 3:
						mvwaddch(gameBox, i, j, ACS_LARROW);
						break;
				}
				wattroff(gameBox, COLOR_PAIR(4));
				break;
			case 'G':
				wattron(gameBox, COLOR_PAIR(5));
				switch(dirG){
					case 0:
						mvwaddch(gameBox, i, j, ACS_UARROW);
						break;
					case 1:
						mvwaddch(gameBox, i, j, ACS_RARROW);
						break;
					case 2:
						mvwaddch(gameBox, i, j, ACS_DARROW);
						break;
					case 3:
						mvwaddch(gameBox, i, j, ACS_LARROW);
						break;
				}
				wattroff(gameBox, COLOR_PAIR(5));
				break;
			}
	wrefresh(gameBox);

}

char CMatrix::WhatsHere(const int y, const int x){
	return matrix[y][x];
}

void CMatrix::UpdatePixel(const int y, const int x, const char Type){
	matrix[y][x] = Type;
}

int CMatrix::FoodLeft(){
	CountFood();
	return foodLeft;
}

int CMatrix::GetRows(){
	return rows;
}

int CMatrix::GetColumns(){
	return columns;
}

string CMatrix::GetNewFilePath(){
	return "../../maps/map1.txt";
}

void CMatrix::CountFood(){
	foodLeft = 0;
	for(int i = 0; i < rows; i++)
		for(int j = 0; j < columns; j++)
			if(matrix[i][j] == '.')
				foodLeft++;
}

void CMatrix::SetGameBoxAddr(WINDOW* win){
	gameBox = win;
}
