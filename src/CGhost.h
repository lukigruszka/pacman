/*
 * CGhost.h
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#ifndef CGHOST_H_
#define CGHOST_H_

#include "CPixel.h"

class CGhost: public CPixel {
public:
	CGhost();
	~CGhost();

	int Move(CMatrix& matrix);
	void StepOnFood(bool);
	bool IsOnFood();
private:
	bool onFood;
};

#endif /* CGHOST_H_ */
