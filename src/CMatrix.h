/*
 * CMatrix.h
 *
 *  Created on: 28.06.2017
 *      Author: luke
 */

#ifndef CMATRIX_H_
#define CMATRIX_H_

#include <string>
#include <ncurses.h>
using namespace std;

class CMatrix {
public:
	CMatrix();
	~CMatrix();

	void LoadMatrixFromFile();
	void ReloadMatrix(int dirP = 1, int dirG = 0);
	char WhatsHere(const int y, const int x);
	void UpdatePixel(const int y, const int x, const char Type);
	int FoodLeft();
	int GetRows();
	int GetColumns();
	void SetGameBoxAddr(WINDOW* win);
private:
	WINDOW* gameBox;
	int foodLeft, rows, columns;
	char **matrix;
	string ActualFilePath;

	void operator--(int);
	string GetNewFilePath();
	void CountFood();
};

#endif /* CMATRIX_H_ */
