/*
 * CPlayer.h
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#ifndef CPLAYER_H_
#define CPLAYER_H_

#include "CPixel.h"
#include <ncurses.h>

class CPlayer: public CPixel {
public:
	CPlayer();
	~CPlayer();

	int Move(WINDOW*, CMatrix& matrix);
};

#endif /* CPLAYER_H_ */
