/*
 * CPlayer.cpp
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#include "CPlayer.h"
#include <ncurses.h>

CPlayer::CPlayer() {


}

CPlayer::~CPlayer() {

}

int CPlayer::Move(WINDOW* gameBox, CMatrix& matrix){
	int tmpC, c;
	int dir;

	switch(this->dir){
		case 0:
			if(matrix.WhatsHere(y-1, x) != '#')
				dir = this->dir;
			else
				dir = (this->dir+2)%4;
			break;
		case 1:
			if(matrix.WhatsHere(y, x+1) != '#')
				dir = this->dir;
			else
				dir = (this->dir+2)%4;
			break;
		case 2:
			if(matrix.WhatsHere(y+1, x) != '#')
				dir = this->dir;
			else
				dir = (this->dir+2)%4;
			break;
		case 3:
			if(matrix.WhatsHere(y, x-1) != '#')
				dir = this->dir;
			else
				dir = (this->dir+2)%4;
			break;
	}

	while(1){
		tmpC = wgetch(gameBox);
		if(tmpC == ERR)
			break;
		c = tmpC;
	}

	switch(c){
		case KEY_UP:
			if(matrix.WhatsHere(y-1, x) != '#')
				dir = 0;
			break;
		case KEY_RIGHT:
			if(matrix.WhatsHere(y, x+1) != '#')
				dir = 1;
			break;
		case KEY_DOWN:
			if(matrix.WhatsHere(y+1, x) != '#')
				dir = 2;
			break;
		case KEY_LEFT:
			if(matrix.WhatsHere(y, x-1) != '#')
				dir = 3;
			break;
	}
	return dir;
}

