/*
 * window.h
 *
 *  Created on: 27.06.2017
 *      Author: luke
 */

#ifndef WINDOW_H_
#define WINDOW_H_

#include "window.h"
#include "CMatrix.h"
#include <ncurses.h>

void 	CursesInit();
void 	ColorsInit();
WINDOW*	DisplayNameBox();
WINDOW*	DisplayMenuBox(int highlight);
WINDOW* DisplayMsgBox();
int 	HandleMenu(WINDOW**);
void 	Play(WINDOW* menuBox);
bool 	TryLoadMap(CMatrix&, WINDOW* msgBox);
void 	DestroyWin(WINDOW*);



#endif /* WINDOW_H_ */
