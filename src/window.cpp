#include "window.h"
#include "CMatrix.h"
#include <fstream>
#include <algorithm>
#include "CScheduler.h"

char *choices[] = {
			"Play",
			"Exit",
		  };
const int n_choices = sizeof(choices) / sizeof(char *);

void ColorsInit(){
	init_pair(1, COLOR_RED, COLOR_BLACK); //Kolor tytułu
	init_pair(2, COLOR_WHITE, COLOR_RED); //Kolor błędu
	init_pair(3, COLOR_CYAN, COLOR_BLACK); //Kolor komunikatów
	init_pair(4, COLOR_GREEN, COLOR_BLACK);//Kolor gracza
	init_pair(5, COLOR_MAGENTA, COLOR_BLACK);//Kolor komputera
	init_pair(6, COLOR_WHITE, COLOR_BLACK);//Kolor zwykly
}

void CursesInit(){
	initscr();
	start_color();
	cbreak();
	keypad(stdscr, TRUE);
	noecho();
	ColorsInit();
}

WINDOW* DisplayNameBox(){
	int screenX = getmaxx(stdscr);

	WINDOW *nameBox;
	nameBox = newwin(3, screenX, 0, 0);
	char title[] = "PacMan Game v1.0 by Lukasz Gruszczelak";

	box(nameBox, 0 , 0);
	wattron(nameBox, COLOR_PAIR(1));
	mvwprintw(nameBox, 1, screenX/2-17, title);
	wattroff(nameBox, COLOR_PAIR(1));
	wrefresh(nameBox);

	return nameBox;
}

WINDOW* DisplayMenuBox(int highlight){
	int screenX = getmaxx(stdscr);

	WINDOW *menuBox;
	menuBox = newwin(n_choices+1, screenX, 4, 2);
	keypad(menuBox, TRUE);

	wprintw(menuBox, "What you want to do?");
	for(int i = 0; i < n_choices; i++){
		if(highlight == i+1){
			wattron(menuBox, A_REVERSE);
			mvwprintw(menuBox, i+1, 3, "%s", choices[i]);
			wattroff(menuBox, A_REVERSE);
		}
		else
			mvwprintw(menuBox, i+1, 3, "%s", choices[i]);
	}

	wrefresh(menuBox);

	return menuBox;
}

WINDOW* DisplayMsgBox(){
	int screenX = getmaxx(stdscr);
	WINDOW* msgBox = newwin(4, 30, 4, screenX-31);
	box(msgBox, '|', '#');

	wrefresh(msgBox);
	return msgBox;

}

void DestroyWin(WINDOW *localWin){
	int winY, winX;

	getmaxyx(localWin, winY, winX);
	for(int y = 0; y < winY; y++)
		for(int x = 0; x < winX; x++)
				mvwaddch(localWin, y, x, ' ');
	wrefresh(localWin);
	delwin(localWin);
}


int HandleMenu(WINDOW** menuBox){
	int highlight=1, ch, choice=0;

	*menuBox = DisplayMenuBox(1); //1 - domyslna podświetlona opcja

	while(1)
	{	ch = wgetch(*menuBox);
		switch(ch)
		{	case KEY_UP:
				if(highlight == 1)
					highlight = n_choices;
				else
					--highlight;
				break;
			case KEY_DOWN:
				if(highlight == n_choices)
					highlight = 1;
				else
					++highlight;
				break;
			case 10:
				choice = highlight;
				break;
		}

		DestroyWin(*menuBox);
		*menuBox = DisplayMenuBox(highlight);
		if(choice != 0)
			break;
	}

	return choice==2 ? 0 : choice;
}

bool TryLoadMap(CMatrix& matrix, WINDOW* msgBox){
	try{
		matrix.LoadMatrixFromFile();
		string line1 = "File containing map";
		string line2 = "loaded successfully";
		wborder(msgBox, ' ', ' ', ' ',' ',' ',' ',' ',' ');
		wrefresh(msgBox);
		wresize(msgBox, 4, max(line1.size(), line2.size())+2);
		box(msgBox, '|', '#');
		wattron(msgBox, COLOR_PAIR(3));
		mvwprintw(msgBox, 1, 1, line1.c_str() );
		mvwprintw(msgBox, 2, 1, line2.c_str());
		wattron(msgBox, COLOR_PAIR(3));
		wrefresh(msgBox);
	}
	catch(const ios_base::failure& e){
		string line1 = "Couldn't open file";
		string line2 = "containing map";
		wborder(msgBox, ' ', ' ', ' ',' ',' ',' ',' ',' ');
		wrefresh(msgBox);
		wresize(msgBox, 4, max(line1.size(), line2.size())+2);
		box(msgBox, '|', '#');
		wattron(msgBox, COLOR_PAIR(2));
		mvwprintw(msgBox, 1, 1, line1.c_str() );
		mvwprintw(msgBox, 2, 1, line2.c_str());
		wattron(msgBox, COLOR_PAIR(2));
		wrefresh(msgBox);
		return true;
	}
	catch(...){
		printw("cos dziwnego sie wydarzylo ");
		return true;
	}
	return false;
}

void Play(WINDOW* menuBox){
	WINDOW* msgBox = DisplayMsgBox();
	CMatrix matrix;
	if(TryLoadMap(matrix, msgBox)){
		DestroyWin(msgBox);
		return;
	}

	WINDOW *gameBox, *infoBox;
	int height, width, x, y, screenX = getmaxx(stdscr);

	getbegyx(menuBox, y, x);
	getmaxyx(menuBox, height, width);
	infoBox = newwin(3, 16, y+height+1, screenX/2-16/2);
	wborder(infoBox, '|', '|', '-', '-', '+', '+', '+', '+');
	mvwprintw(infoBox, 1, 1, "Food left: %d", matrix.FoodLeft());
	wrefresh(infoBox);
	refresh();

	getbegyx(infoBox, y, x);
	getmaxyx(infoBox, height, width);
	gameBox = newwin(matrix.GetRows(), matrix.GetColumns(), y+height+1, screenX/2-matrix.GetColumns()/2);
	box(gameBox, 0, 0);
	wrefresh(gameBox);
	matrix.SetGameBoxAddr(gameBox);
	keypad(gameBox, TRUE);
	nodelay(gameBox, TRUE);

	//scheduler
	CScheduler sched;
	sched.Run(gameBox, infoBox, matrix);


	getch();
	DestroyWin(msgBox);
	DestroyWin(infoBox);
	DestroyWin(gameBox);
}
