#include "window.h"
#include "CMatrix.h"


int main(){
	WINDOW *nameBox, *menuBox;
	int choice;

	CursesInit();
	refresh();


	curs_set(0);
	nameBox = DisplayNameBox();

	choice = HandleMenu(&menuBox);
	while(choice){
		switch(choice){
		case 1:
			Play(menuBox);
			DestroyWin(menuBox);
		}
		choice = HandleMenu(&menuBox);
	}

	DestroyWin(menuBox);
	DestroyWin(nameBox);
	endwin();
	return 0;
}
