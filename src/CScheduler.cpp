/*
 * CScheduler.cpp
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#include "CScheduler.h"
#include <unistd.h>
#include "CPlayer.h"
#include "CGhost.h"
#include "CMatrix.h"


CScheduler::CScheduler(){

}

CScheduler::~CScheduler() {
}


void CScheduler::Win(WINDOW* infoBox){
	wattron(infoBox, COLOR_PAIR(4));
	mvwprintw(infoBox, 1, 1, "YOU WIN");
	wattroff(infoBox, COLOR_PAIR(4));
	wrefresh(infoBox);
}

void CScheduler::Lose(WINDOW* infoBox){
	wattron(infoBox, COLOR_PAIR(2));
	mvwprintw(infoBox, 1, 1, "YOU LOSE");
	wattroff(infoBox, COLOR_PAIR(2));
	wrefresh(infoBox);
}


void CScheduler::Run(WINDOW* gameBox, WINDOW* infoBox, CMatrix& matrix){
	CGhost ghost;
	CPlayer player;
	int newGhostX, newGhostY, newPlayerX, newPlayerY;

	for(int i = 0; i < matrix.GetRows(); i++){ //znajdź początkowe położenia
		for(int j = 0; j < matrix.GetColumns(); j++)
			if(matrix.WhatsHere(i, j) == 'P'){
				player.SetY(i);
				player.SetX(j);
			}
			else if(matrix.WhatsHere(i, j) == 'G'){
				ghost.SetY(i);
				ghost.SetX(j);
			}
	}

	matrix.ReloadMatrix();
	while(1){
		if(!matrix.FoodLeft()){
			Win(infoBox);
			usleep(1000*1000);
			getch();
			return;
		}
		mvwprintw(infoBox, 1, 1, "Food left: %d", matrix.FoodLeft());
		wrefresh(infoBox);

		ghost.SetDirection(ghost.Move(matrix));
		switch(ghost.GetDirection()){
		case 0:
			newGhostX = ghost.GetX();
			newGhostY = ghost.GetY()-1;
			break;
		case 1:
			newGhostX = ghost.GetX()+1;
			newGhostY = ghost.GetY();
			break;
		case 2:
			newGhostX = ghost.GetX();
			newGhostY = ghost.GetY()+1;
			break;
		case 3:
			newGhostX = ghost.GetX()-1;
			newGhostY = ghost.GetY();
			break;
		}

		player.SetDirection(player.Move(gameBox, matrix));
		switch(player.GetDirection()){
		case 0:
			newPlayerX = player.GetX();
			newPlayerY = player.GetY()-1;
			break;
		case 1:
			newPlayerX = player.GetX()+1;
			newPlayerY = player.GetY();
			break;
		case 2:
			newPlayerX = player.GetX();
			newPlayerY = player.GetY()+1;
			break;
		case 3:
			newPlayerX = player.GetX()-1;
			newPlayerY = player.GetY();
			break;
		}

		if((ghost.GetX() == newPlayerX && ghost.GetY() == newPlayerY && player.GetX() == newGhostX && player.GetY() == newGhostX) || (newGhostY == newPlayerY && newGhostX == newPlayerX)){
			Lose(infoBox);
			usleep(1000*1000);
			getch();
		return;
	}


		wattron(gameBox, COLOR_PAIR(6));
		matrix.UpdatePixel(ghost.GetY(), ghost.GetX(), ghost.IsOnFood() ? '.' : ' ');
		wattron(gameBox, COLOR_PAIR(5));
		ghost.StepOnFood(matrix.WhatsHere(newGhostY, newGhostX) == '.' ? true : false);
		matrix.UpdatePixel(newGhostY, newGhostX, 'G');
		ghost.SetX(newGhostX);
		ghost.SetY(newGhostY);
		wattroff(gameBox, COLOR_PAIR(5));

		matrix.UpdatePixel(player.GetY(), player.GetX(), ' ');
		wattron(gameBox, COLOR_PAIR(4));
		matrix.UpdatePixel(newPlayerY, newPlayerX, 'P');
		player.SetX(newPlayerX);
		player.SetY(newPlayerY);
		wattroff(gameBox, COLOR_PAIR(4));
		matrix.ReloadMatrix(player.GetDirection(), ghost.GetDirection());
		usleep(250*1000);
	}
}
