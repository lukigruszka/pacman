/*
 * CPixel.h
 *
 *  Created on: 28.06.2017
 *      Author: root
 */

#ifndef CPIXEL_H_
#define CPIXEL_H_

#include "CMatrix.h"

class CPixel {
public:
	CPixel();
	virtual ~CPixel();

	virtual int Move(CMatrix&);
	void Disappear();
	int GetX();
	int GetY();
	void SetX(int);
	void SetY(int);
	bool IsAlive();
	int GetDirection();
	void SetDirection(int dir);
protected:
	int dir;
	bool alive;
	int x, y;
};

#endif /* CPIXEL_H_ */
